$(function(){
	
    /*
    var $win = $(window)
      , $nav = $('.subnav')
      , navTop = $('.subnav').length && $('.subnav').offset().top - 40
      , isFixed = 0

    processScroll();

    $win.on('scroll', processScroll)

    function processScroll() {
      var i, scrollTop = $win.scrollTop()
      if (scrollTop >= navTop && !isFixed) {
        isFixed = 1
        $nav.addClass('subnav-fixed')
      } else if (scrollTop <= navTop && isFixed) {
        isFixed = 0
        $nav.removeClass('subnav-fixed')
      }
    }
   	*/
   	
   	$("#addCamp").click(function(){
           if(content === "cg"){
               $('#newCampFrm').modal('toggle');
           } else {
               $("#cg").empty();
               getAllCampgrounds();
               $('#newReviewFrm').modal('toggle');
           }

	});
	
	$("#clsNewCampFrm").click(function(){
		$('#newCampFrm').modal('toggle');
	});
	
	$("#clsEditCampFrm").click(function(){
		$('#editCampFrm').modal('toggle');
	});

    $("#clsSaveReview").click(function(){
        $('#newReviewFrm').modal('toggle');
    });
	
	$('#saveCamp').click(function(){
		$('#infoPnl').toggle();
		$.post("data/savecamp.php", $("#campFrm").serialize(),
  			function(data){
    			//alert("Data Loaded: " + data);
    			if(data == 1){
    				$('#infoPnl').addClass('alert alert-success');
    				$('#infoPnl').html('<strong>Campground Saved</strong>');
    			} else {
    				$('#infoPnl').addClass('alert alert-error');
    				$('#infoPnl').html('<strong>ERROR!</strong> Something went wrong. Please try again.');
    			}
    			
    		}
		);
	});

    $('#editCamp').click(function(){
        $('#infoPnl2').toggle();
        $.post("data/updatecamp.php", $("#editFrm").serialize(),
            function(data){
                //getNextCampgrounds(campStart,campEnd);
                //$('#editCampFrm').modal('toggle');
                //alert("Data Loaded: " + data);
                if(data == 1){
                    $('#infoPnl2').addClass('alert alert-success');
                    $('#infoPnl2').html('<strong>Campground Saved</strong>');
                } else {
                    $('#infoPnl2').addClass('alert alert-error');
                    $('#infoPnl2').html('<strong>ERROR!</strong> Something went wrong. Please try again.');
                }

            }
        );
    });

    $('#updateReview').click(function(){
        $('#editRevPnl').toggle();
        $.post("data/updatereview.php", $("#editrvFrm").serialize(),
            function(data){
                //getNextCampgrounds(campStart,campEnd);
                //$("#editCampFrm").toggle();
                alert("Data Loaded: " + data);
                if(data == 1){
                    $('#editRevPnl').addClass('alert alert-success');
                    $('#editRevPnl').html('<strong>Campground Saved</strong>');
                } else {
                    $('#editRevPnl').addClass('alert alert-error');
                    $('#editRevPnl').html('<strong>ERROR!</strong> Something went wrong. Please try again.');
                }

            }
        );
    });

    $('#saveReview').click(function(){
        $('#newrevPnl').toggle();
        $.post("data/savereview.php", $("#newrvFrm").serialize(),
            function(data){
                //alert("Data Loaded: " + data);
                if(data == 1){
                    $('#newrevPnl').addClass('alert alert-success');
                    $('#newrevPnl').html('<strong>Campground Saved</strong>');
                } else {
                    $('#newrevPnl').addClass('alert alert-error');
                    $('#newrevPnl').html('<strong>ERROR!</strong> Something went wrong. Please try again.');
                }

            }
        );
    });
   	
	getNextCampgrounds(0,100);
	$('#cgmnu').addClass('active');
	getTotalCamps();
	
});

var total = 0;
var campStart = 0;
var campEnd = 100;
var revStart = 0;
var revEnd = 100;
var content = 'cg';
var geocoder = new google.maps.Geocoder();

function codeAddress(address) {
    
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
       /*
       map.setCenter(results[0].geometry.location);
        //console.log(results[0].geometry.location.a$);
        var marker = new google.maps.Marker({
            map: map, 
            position: results[0].geometry.location
        });
        */
       // console.log(results[0].geometry.location);
       //ret = {"lat":results[0].geometry.location.$a,"long":results[0].geometry.location.ab};
  
      } else {
        alert("Geocode was not successful for the following reason: " + status);
      }
      
    });
    
    
}

function getTotalCamps(){
	$('#pageLinks').empty();
	$.getJSON('data/totalcamps.php', function(data) {
	  	//alert(data.total);
	  	total = data.total;
	  	//alert(total/100);
	  	pages = total/100;
	  	bump = 0;
	  	for(var i = 0;i<pages;i++){
	  		bump = i * 100;
	  		$('#pageLinks').append('<li><a onclick="getNextCampgrounds('+bump+',100);" style="cursor:pointer">'+(i+1)+'</a></li>');
	  		
	  	}
	  	return pages;
	});
}

function getTotalReviews(){
	$('#pageLinks').empty();
	$.getJSON('data/totalreviews.php', function(data) {
	  	//alert(data.total);
	  	total = data.total;
	  	//alert(total/100);
	  	pages = total/200;
	  	bump = 0;
	  	for(var i = 0;i<pages;i++){
	  		bump = i * 200;
	  		$('#pageLinks').append('<li><a onclick="getNextReviews('+bump+',200);" style="cursor:pointer">'+(i+1)+'</a></li>');
	  		
	  	}
	  	return pages;
	});
}

function getAllCampgrounds(which,where){
    $.getJSON('data/allcampgrounds.php', function(data) {

        $.each(data.camps, function(i,item){
            var sel = '';
            if(which === item.camp.number){
                sel = ' selected';
            }

            if(where){
                $('#'+where).append('<option value="'+item.camp.number+'" '+sel+'>'+item.camp.name+'</li>');
            } else {
                $('#cg').append('<option value="'+item.camp.number+'" '+sel+'>'+item.camp.name+'</li>');
            }


        });
        //return pages;
    });
}

function getNextCampgrounds(start,end){
    campStart = start;
    campEnd = end;

	$("#listBody").empty();
	$.getJSON('data/campgrounds.php?s='+start+'&e='+end, function(data) {
	  	var items = [];
		//console.log(data.camps.length);
   		$.each(data.camps, function(i,item){
    		$('#listBody').append('<tr><td width="80"><div class="btn-group"><a class="btn btn-mini btn-primary" onclick="editCamp('+item.camp.number+');">Edit</a><a class="btn dropdown-toggle btn-mini btn-inverse" data-toggle="dropdown" href="#"><span class="caret"></span></a><ul class="dropdown-menu"><li><a href="#">Delete</a></li><li><a href="#">Features</a></li></ul></div></td><td>'+item.camp.name+'</td><td>'+item.camp.city+', '+item.camp.state+'</td><td>'+item.camp.website+'</td><td>'+item.camp.geoLat+' / '+item.camp.geoLang+'</td></tr>');
  		});
	});
}

function getNextReviews(start,end){
    revStart = start;
    revEnd = end;

	$("#rvlistBody").empty();
	$.getJSON('data/reviews.php?s='+start+'&e='+end, function(data) {
	  	var items = [];
		
   		$.each(data.reviews, function(i,item){
    		$('#rvlistBody').append('<tr><td width="80"><div class="btn-group"><a class="btn btn-mini btn-primary" onclick="editReview('+item.review.id+');">Edit</a><a class="btn dropdown-toggle btn-mini btn-inverse" data-toggle="dropdown" href="#"><span class="caret"></span></a><ul class="dropdown-menu"><li><a href="#">Delete</a></li><li><a href="#">Features</a></li></ul></div></td><td>'+item.review.name+'</td><td>'+item.review.camp+'</td></tr>');
  		});
	});
}

function searchCamps(){
	alert($("#campName").val());
	if($('#campName').val() != "undefined" && $('#campName').val() != ""){
		$.post("data/searchcamps.php", $("#campSearchFrm").serialize(),
			function(data){
				//alert("Data Loaded: " + data);
				jdata = $.parseJSON(data);
				//alert(jdata.camps.length);
				
				total = jdata.camps.length;
			  	//alert(total/100);
			  	pages = total/100;
			  	bump = 0;
			  	for(var i = 0;i<pages;i++){
			  		bump = i * 100;
			  		$('#pageLinks').append('<li><a onclick="getNextCampgrounds('+bump+',100);" style="cursor:pointer">'+(i+1)+'</a></li>');
			  		
			  	}
			  	
			  	var items = [];
				$('#listBody').empty();
		   		$.each(jdata.camps, function(i,item){
		    		$('#listBody').append('<tr><td width="80"><div class="btn-group"><a class="btn btn-mini btn-primary" href="#">Edit</a><a class="btn dropdown-toggle btn-mini btn-inverse" data-toggle="dropdown" href="#"><span class="caret"></span></a><ul class="dropdown-menu"><li><a href="#">Delete</a></li><li><a href="#">Features</a></li></ul></div></td><td>'+item.camp.name+'</td><td>'+item.camp.city+', '+item.camp.state+'</td><td>'+item.camp.website+'</td></tr>');
		  		});
			}
		);
		return false;
	} else {
		getTotalReviews();
		getNextReviews(0,200);	
	}
}

function updateCampGeo(camp){
	//var geo = codeAddress(where);
	console.log(camp);
	//$.get('data/updatecampgeo.php?c='+camp+'&lat='+geo
}

function editCamp(what){
    //alert(what);
    $.getJSON('data/campground.php?c='+what, function(data) {
        var items = [];
        	var lat = '';
        	var lang = '';
         $.each(data.data[0].camps, function(i,item){
         	geocoder.geocode( { 'address': item.camp.address + ' ' + item.camp.city + ' ' + item.camp.state}, function(results, status) {
		      if (status == google.maps.GeocoderStatus.OK) {
		       /*
		       map.setCenter(results[0].geometry.location);
		        //console.log(results[0].geometry.location.a$);
		        var marker = new google.maps.Marker({
		            map: map, 
		            position: results[0].geometry.location
		        });
		        */
		       // console.log(results[0].geometry.location);
		       lat = results[0].geometry.location.$a;
		       lang = results[0].geometry.location.ab;
		  
		      } else {
		        alert("Geocode was not successful for the following reason: " + status);
		      }
		      
			    //console.log(lat);
	
	            $('#num').val(item.camp.number);
	            $('#ecgname').val(item.camp.name);
	            $('#ecgaddy').val(item.camp.address);
	            $('#ecgcity').val(item.camp.city);
	            $('#ecgstate').val(item.camp.state);
	            $('#ecgzip').val(item.camp.zip);
	            $('#ecgphone').val(item.camp.phone);
	            $('#ecgweb').val(item.camp.website);
	            $('#ecgemail').val(item.camp.email);
	            $('#ecglat').val(lat);
	            $('#ecglong').val(lang);
            
            });
            
		 });
		    
         
    });
    $('#editCampFrm').modal('toggle');
}

function editReview(what){
    $.getJSON('data/review.php?r='+what, function(data) {
        var items = [];

        $.each(data.reviews, function(i,item){
            $('#revnum').val(item.review.id);
            $('#ecg').val(item.review.name);
            $('#erevname').val(item.review.name);
            $('#edateOfStay').val(item.review.date_of_stay);
            $('#esitenum').val(item.review.site_number);
            $('#erevrating').val(item.review.rating);
            $('#ecomments').val(item.review.comments);
            $('#estayagain').val(item.review.stay_again);
            getAllCampgrounds(item.review.number,'ecg');
        });
    });
    $('#editReviewFrm').modal('toggle');
}

function switchContent(what){
//alert(what);
    content = what;
	switch(what){
		case "cg":
			$("#cgmnu").addClass('active');
			$("#revmnu").removeClass('active');
			$("#rvrmnu").removeClass('active');
			$("#adminmnu").removeClass('active');
			$("#cgs").show();
			$("#2").hide();
			$("#3").hide();
			$("#4").hide();
			getTotalCamps();
			break;
		case "reviews":
			$("#cgmnu").removeClass('active');
			$("#revmnu").addClass('active');
			$("#rvrmnu").removeClass('active');
			$("#adminmnu").removeClass('active');
			$("#cgs").hide();
			$("#2").show();
			$("#3").hide();
			$("#4").hide();
			getNextReviews(0,200);
			getTotalReviews();
			break;
		case "reviewers":
			$("#cgmnu").removeClass('active');
			$("#revmnu").removeClass('active');
			$("#rvrmnu").addClass('active');
			$("#adminmnu").removeClass('active');
			$("#cgs").hide();
			$("#2").hide();
			$("#3").show();
			$("#4").hide();
			break;
		case "admins":
			$("#cgmnu").removeClass('active');
			$("#revmnu").removeClass('active');
			$("#rvrmnu").removeClass('active');
			$("#adminmnu").addClass('active');
			$("#cgs").hide();
			$("#2").hide();
			$("#3").hide();
			$("#4").show();
			break;
	}
}

