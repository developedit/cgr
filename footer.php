 <div class="clear"></div>

  </div><!-- #main -->

  <div id="footer-container">
    <footer>
      
      <div id="footerlinks" class="column">
        <h4>Quicklinks</h4>
        <ul>
          <li id="subreview_lnk"><a href="#">Submit a Review</a></li>
          <li><a href="#">Claim a Campground</a></li>
          <li><a href="#">Sitemap</a></li>
          <li><a href="#">Contact Us</a></li>
          <li><a href="#">Privacy Policy</a></li>
        </ul>
      </div><!-- #footerlinks -->

      <div id="footer_right">
        <img src="<?php echo $basePath; ?>/images/logo.png" width="268" height="103" alt="Campground Report" />
        <p>Copyright &copy; 2000-2012 Campground Report. A <a href="http://www.socialknowledge.com">Social Knowledge</a> Site</p>
      </div><!-- #footer_right -->

      <div class="clear"></div>

    </footer>
  </div>

  <script src="<?php echo $basePath; ?>/js/jquery.js"></script>
  <script src="<?php echo $basePath; ?>/js/jquery-ui-1.8.18.custom.min.js"></script>
  <script src="<?php echo $basePath; ?>/js/tinybox.js"></script>
  <script src="<?php echo $basePath; ?>/js/common.js"></script>

<script>
  var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']]; // Change UA-XXXXX-X to be your site's ID
  (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];g.async=1;
  g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
  s.parentNode.insertBefore(g,s)}(document,'script'));
</script>

<!--[if lt IE 7 ]>
  <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.2/CFInstall.min.js"></script>
  <script>window.attachEvent("onload",function(){CFInstall.check({mode:"overlay"})})</script>
<![endif]-->

</body>
</html>