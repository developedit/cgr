<?php include 'header.php'; ?>

<div id="main" class="wrapper">

<div id="left_column">
    <?php include 'main_content.php'; ?>
</div><!-- #left_column -->

<?php include 'sidebar_main2.php'; ?>
<div class="clear"></div>

</div><!-- #main -->


<div id="dialog-modal" title="Warning">
    <p>You must login or register before submitting a review.</p>
</div>
<?php include 'footer.php'; ?>
<script src="js/app.js"></script>