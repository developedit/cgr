
    <div id="page_title">
      <h1>Add a Campground Review</h1>
      <p class="state_description">Please be as accurate as possible in your review.</p>
    </div><!-- #state_title -->
      <div class="ui-state-highlight ui-corner-all" id="review_div" style="margin-top: 20px; padding: 0 .7em;display:none">
          <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
              <ul id="review_msg"></ul></p>
      </div>

    <form id="newReview_frm">
	    <input type="hidden" name="reviewer" id="reviewer"/>
      <fieldset>
        <h2>Choose a Campground</h2>

            <label for="country">Country<em>*</em></label>
              <select id="country" name="country" class="form-text">
                  <option value="nothing" selected="selected">- please select -</option>
                <option value="USA">United States</option>
                <option value="CA">Canada</option>
                <option value="MX">Mexico</option>
              </select>
      </fieldset>
      <fieldset>
            <label for="state">State<em>*</em></label>
            <select id="state" name="state" onchange="getStateCampgrounds()">

            </select>
      </fieldset>
      <fieldset>
        <label for="revname">Campground Name<em>*</em></label>
            <select id="cg" name="cg">

            </select>

      </fieldset>

      <fieldset>
        <h2>Review Details</h2>

            <label for="month_of_stay">Month of Stay<em>*</em></label>
            <select class="month" id="month_of_stay" name="month_of_stay">
              <option value="nothing" selected="selected">- please select -</option>
              <option value="01">January</option>
              <option value="02">February</option>
              <option value="03">March</option>
              <option value="04">April</option>
              <option value="05">May</option>
              <option value="06">June</option>
              <option value="07">July</option>
              <option value="08">August</option>
              <option value="09">September</option>
              <option value="10">October</option>
              <option value="11">November</option>
              <option value="12">December</option>
            </select>
      </fieldset>
        <fieldset>
            <label for="year_of_stay">Year of Stay<em>*</em></label>
            <select class="year" id="year_of_stay" name="year_of_stay">
              <option value="nothing" selected="selected">- please select -</option>
              <option value="2012">2012</option>
              <option value="2011">2011</option>
              <option value="2010">2010</option>
              <option value="2009">2009</option>
              <option value="2008">2008</option>
              <option value="2007">2007</option>
              <option value="2006">2006</option>
            </select>
        </fieldset>
        <fieldset>
            <label for="rate">Rate Paid ($)<em>*</em></label>
            <input type="text" id="rate" name="rate" class="form-text"/>
        </fieldset>
        <fieldset>
            <label for="sitetype">Site Type</label>
            <select class="sitetype" id="sitetype" name="sitetype">
              <option value="nothing" selected="selected">- please select -</option>
              <option value="rv">RV</option>
              <option value="tent">Tent</option>
              <option value="cabin">Cabin / Yurt</option>
              <option value="groupsite">Group Site / Lodge</option>
              <option value="other">other</option>
            </select>
        </fieldset>
      <fieldset>
        <h2>Review &amp; Rating</h2>

            <label for="rating">Your Rating<em>*</em></label>
            <select class="year" name="rating" id="rating">
             <option value="nothing" selected="selected">- please select -</option>
                <option value="nothing" selected="selected">- please select -</option>
              <option value="5">5 - Best</option>
              <option value="4">4</option>
              <option value="3">3</option>
              <option value="2">2</option>
              <option value="1">1 - Worst</option>
            </select>
      </fieldset>
      <fieldset>
      	<label for="stayagain">Stay Again<em>*</em></label>
        <select id="stayagain" name="stayagain">
        	<option value="1">Yes</option>
            <option value="0">No</option>
        </select>
      </fieldset>
        <fieldset>
            <label for="review">Your Review</label>
            <textarea name="review" id="review" cols="75" rows="8"></textarea>
        </fieldset>
        <fieldset>
            <input type="button" onclick="saveReview();" value="Submit Review"/>
      </fieldset>

    </form>
