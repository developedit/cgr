    <aside>

      <section id="social_icons">
        <ul>
          <li><a href="#"><img src="images/icon_ios.png" alot="Campground Report iOS App" /></a></li>
          <li><a href="#"><img src="images/icon_android.png" alot="Campground Report Android App" /></a></li>
          <li><a href="#"><img src="images/icon_facebook.png" alot="Campground Report on Facebook" /></a></li>
          <li><a href="#"><img src="images/icon_gplus.png" alot="Campground Report on Google Plus" /></a></li>
      </section>
      <?php include('login_frm.php'); ?>
      <section id="submit_review">
        <h3>Submit a Review</h3>
        <p>Submitting Reviews is Free, Easy, and helps to keep Campground Report a great place to find any information for later camping trips.</p>
        <div class="submit_button">
          <a id="subreview" class="submit"><span>Submit a Review</span></a>
        </div>
      </section>

      <section id="featured_member" class="clearfix">
        <h3>Featured Member</h3>

        <div>
          <img class="featured_member" src="images/avatar.jpg" alt="Members Photo" />
          <a class="username_large" href="#">Jeremy</a><br />
          <p class="side_location">Detroit, MI</p>
          <p class="side_reviews">Reviews: <a href="#">19</a> | Check-Ins: <a href="#">9</a></p>
        </div>

      </section>

    </aside>