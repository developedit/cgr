<!-- <div id="left_column"> -->
    <div id="page_title">
      <h1>Join Campground Report</h1>
    </div><!-- #state_title -->
      <div id="reg_alert" style="display: none;">
         Registration Complete!
      </div>
    <section>
      <form id="register_frm">
        <fieldset>
          <label for="name">Username</label>
          <input type="text" name="name" id="name" class="form-text" onblur="checkUserName();"/>
          <p class="form-help">Enter your Desired Username.</p>
        </fieldset>
        
        <fieldset id="pwd_fs">
          <label for="password">Password</label>
          <input type="password" name="password" id="password" class="form-text"/>
          <p class="form-help">Choose a Password.</p>
        </fieldset>

        <fieldset id="conf_pwd_fs">
          <label for="confirm-password">Confirm Password</label>
          <input type="password" id="confirm-password" class="form-text" onblur="checkpwd()"/>
          <p class="form-help">Confirm your Password.</p>
        </fieldset>

        <fieldset>
          <label for="email">Email</label>
          <input type="email" name="email" id="email" class="form-text" />
        </fieldset>
        
        <fieldset>
          <label for="gender">Gender</label>
          <select id="gender">
            <option value="M">Male</option>
            <option value="F">Female</option>
            <option value="O">Other</option>
          </select>
        </fieldset>

        <fieldset>
          <label for="bio">Bio</label>
          <textarea id="bio" name="bio"></textarea>
        </fieldset>
        
        <fieldset class="check">
          <label><input type="checkbox" name="terms" value="1"/> &mdash; I accept the terms of service.</label>
        </fieldset>

        <fieldset class="form-actions">
        	<button id="cancel_btn">Cancel</button>
          <input type="button" value="Submit" id="login_btn" onclick="register()"/>
        </fieldset>
      </form>     
    </section>
<div class="clear"></div>