<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<?php include_once('config.php'); ?>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>Campground Reviews and RV Park Reviews</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <meta name="viewport" content="width=device-width,initial-scale=1">

<link rel="stylesheet" href="<?php echo $basePath; ?>/css/trontastic/jquery-ui-1.8.18.custom.css">
  <link rel="stylesheet" href="<?php echo $basePath; ?>/css/style2.css">
<!--  <link rel="stylesheet" href="--><?php //echo $basePath; ?><!--/css/new_cr_sidebar_login.css">-->
<!--  <link rel="stylesheet" href="--><?php //echo $basePath; ?><!--/css/cr_additional_style.css">-->
  <!-- map api -->
  <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBxbb7btwaq4ADW_y5cqQlM5GV2s3pyYB0&sensor=false"></script>
  <script src="<?php echo $basePath; ?>/js/libs/modernizr-2.0.min.js"></script>
  <script src="<?php echo $basePath; ?>/js/libs/respond.min.js"></script>
</head>
<body>

  <div id="header-container">
    <header class="wrapper">
      <div id="logo">
        <a href="<?php echo $basePath; ?>"><img src="<?php echo $basePath; ?>/images/logo.png" width="268" height="103" alt="Campground Report"/></a>
      </div><!-- #logo --> 

      <div id="header_search">
          <form id="searchform">
            <fieldset class="search">
              <input type="text" name="searchterm" value="Search for a Campground, City, or State" class="box" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;" />
              <button class="btn" title="Submit Search" id="searchBtn">Search</button>
            </fieldset>
          </form>
      </div><!-- #header_search -->

      <div class="clear"></div>
      
        <nav>
          <ul>
            <li><a id="home_tab" onclick="showMain();" class="current">Home</a></li>
            <li><a id="state_tab" onclick="showState();">States</a></li>
            <!-- <li><a id="region_tab" href="#">Regions</a></li> -->
              <li class="register" id="register">
                  <a href="<?php echo $basePath; ?>/registration.php" id="login_url"><img src="<?php echo $basePath; ?>/images/register.png" alt="Register" id="login_reg"></a>
              </li>
              <li id="logout_btn" class="register" style="display: none;">
                  <a href="#" onclick="logout();"><img src="<?php echo $basePath; ?>/images/logout.png"></a>
              </li>
          </ul>
        </nav>
    </header>
  </div>