    <?php include('header.php'); ?>
<div id="main_no_aside" class="wrapper">

  <div id="full_column">

    <div id="page_title">
      <h1 id="camp_name">Coles Creek State Park</h1>
      <p id="page_location" class="page_location"><a href="#">Long Lake</a>, <a href="#">New York</a> <span class="claim"><a href="#">Claim Campground</a></span></p>
    </div><!-- #page_title -->

    <section id="campground_overview" class="clearfix">
      <div class="overview_wrap">
      <h2>Campground Overview</h2>

        <div class="cg_description">
          <p id="camp_desc">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
        </div><!-- .cg_description -->

        <div class="cg_meta" id="camp_addy">
          <p>Route 37<br />
          Waddington, NY 13694<br />
          (315) 388-5636<br />
          <a target="_blank" href="#">Visit Website →</a></p>
        </div><!-- .cg_meta -->

      </div><!-- .overview_wrap -->

      <div class="cg_map" id="cg_map">
	      <iframe id="imap" width="290" height="150" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src=""></iframe>
      </div><!-- .cg_map -->

    </section><!-- #campground_overview -->

    <section id="cg_reviews">

      <h2>Coles Creek State Park Reviews</h2>

        <article class="review clearfix">
          <div class="userinfo">
            <img class="featured_member" src="images/avatar.jpg" alt="Members Photo" /><br />
            <a class="username" href="#">Jeremy</a><br />
            Reviews: <a href="#">19</a>
          </div><!-- .userinfo -->

          <div class="article_content">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est ...</p>
            <img class="rating" src="images/fire/four-half.png" /> <span class="date_of_stay">Date of Stay: 10/2011 via Tent</span> <img src="images/thumbs_up.png" /> <img src="images/thumbs_down.png" /> 
          </div><!-- article_content -->
        </article>

        <article class="review">
          <div class="userinfo">
            <img class="featured_member" src="images/avatar.jpg" alt="Members Photo" /><br />
            <a class="username" href="#">Jeremy</a><br />
            Reviews: <a href="#">19</a>
          </div><!-- .userinfo -->

          <div class="article_content">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est ...</p>
            <img class="rating" src="images/fire/four-half.png" /> <span class="date_of_stay">Date of Stay: 10/2011 via Tent</span> <img src="images/thumbs_up.png" /> <img src="images/thumbs_down.png" /> 
          </div><!-- article_content -->
        </article>
      
    </section><!-- #cg_reviews -->

    <section id="cg_photos">
      <h2>Official Photos</h2>
      <img class="cg_official" src="<?php echo $basePath;?>/images/official-photo.jpg" alt="Official Campground Photo" /><br />

      <h3>Guest Submitted Photos</h3>
      <img class="cg_official" src="<?php echo $basePath;?>/images/official-photo.jpg" width="90" height="65" alt="Official Campground Photo" /> <img class="cg_official" src="<?php echo $basePath;?>/images/official-photo.jpg" width="90" height="65" alt="Official Campground Photo" /> <img class="cg_official" src="<?php echo $basePath;?>/images/official-photo.jpg" width="90" height="65" alt="Official Campground Photo" />

    </section><!-- #cg_photos -->

    <section class="datatable">
      <h3>General Info</h3>
      <table class="cg_tables" border="1" cellpadding="4" id="optionTable"></table>
    </section>

    <!--
<section class="datatable">
      <h3>General Info</h3>
      <table class="cg_tables" border="1" cellpadding="4">
        <tr>
          <td>Number of Sites</td>
          <td class="right_value">227</td>
        </tr>
        <tr>
          <td>Pull Through Sites</td>
          <td class="right_value">52</td>
        </tr>
        <tr>
          <td>Full Hookup Sites</td>
          <td class="right_value">52</td>
        </tr>
        <tr>
          <td>Shaded Sites</td>
          <td class="right_value">Yes</td>
        </tr>
        <tr>
          <td>Pets Allowed</td>
          <td class="right_value">No</td>
        </tr>
        <tr>
          <td>Big Rigs</td>
          <td class="right_value">No</td>
        </tr>
        <tr>
          <td>Tent Sites / Allowed</td>
          <td class="right_value">No</td>
        </tr>
        <tr>
          <td>Cabins/Cottages/Yurts</td>
          <td class="right_value">Yes</td>
        </tr>
        <tr>
          <td>Group Facilities</td>
          <td class="right_value">Yes</td>
        </tr>
        <tr>
          <td>Unique Accomodations</td>
          <td class="right_value">text here</td>
        </tr>

      </table>
    </section>

    <section class="datatable right">
      <h3>General Info</h3>
      <table class="cg_tables" border="1" cellpadding="4">
        <tr>
          <td>Number of Sites</td>
          <td class="right_value">227</td>
        </tr>
        <tr>
          <td>Pull Through Sites</td>
          <td class="right_value">52</td>
        </tr>
        <tr>
          <td>Full Hookup Sites</td>
          <td class="right_value">52</td>
        </tr>
        <tr>
          <td>Shaded Sites</td>
          <td class="right_value"><img src="images/icon_yes.png" alt="yes" /></td>
        </tr>
        <tr>
          <td>Pets Allowed</td>
          <td class="right_value"><img src="images/icon_yes.png" alt="yes" /><img src="images/icon_fee.png" alt="additional fee" /></td>
        </tr>
        <tr>
          <td>Big Rigs</td>
          <td class="right_value"><img src="images/icon_no.png" alt="no" /></td>
        </tr>
        <tr>
          <td>Tent Sites / Allowed</td>
          <td class="right_value"><img src="images/icon_no.png" alt="no" /></td>
        </tr>
        <tr>
          <td>Cabins/Cottages/Yurts</td>
          <td class="right_value"><img src="images/icon_yes.png" alt="yes" /></td>
        </tr>
        <tr>
          <td>Group Facilities</td>
          <td class="right_value"><img src="images/icon_yes.png" alt="yes" /></td>
        </tr>
        <tr>
          <td>Unique Accomodations</td>
          <td class="right_value">text here</td>
        </tr>

      </table>
    </section>
-->

    <div class="clear"></div>

    </div><!-- #full_column -->
    <?php include('footer.php'); ?>
    
<script>
    var basePath = 'http://dev.campgroundreport.com/';
//var basePath = 'http://127.0.0.1/camping/';
    var sPath = window.location.pathname;
    var sPage = sPath.substring(sPath.lastIndexOf('/') + 1);
    var camp = sPage;
    camp = camp.replace(/-/g, ' ');
    //console.log(camp);
    var cgMap;

//$(function(){
	getCampgroundData(camp);
//});

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

function getCampgroundData(camp){
	console.log(camp);
	$.getJSON(basePath+'data/campground.php?c='+camp, function(data) {
        
        $("#camp_name").html(data.data[0].camps[0].camp.name);
        $("#page_location").html(data.data[0].camps[0].camp.city + ', '+ data.data[0].camps[0].camp.state);
        $("#camp_desc").html(data.data[0].camps[0].camp.description);
        $("#camp_addy").html('<p>'+data.data[0].camps[0].camp.address+'<br />'+data.data[0].camps[0].camp.city + ', '+data.data[0].camps[0].camp.state + ' ' + data.data[0].camps[0].camp.zip + '<br />'+data.data[0].camps[0].camp.phone+'<br /><a target="_blank" href="'+data.data[0].camps[0].camp.website+'">Visit Website →</a></p>');
        
        $("#cg_reviews").empty();
        
        var campName = data.data[0].camps[0].camp.name;
        var cid = data.data[0].camps[0].camp.number;
        //codeAddress(campName);
        campName = campName.replace(/\s/g,' ');
        campNameLwr = campName.toLowerCase();
        
        $("#imap").attr('src',"http://maps.google.com/maps?ie=UTF8&q="+campNameLwr+"&fb=1&gl=us&hq="+campNameLwr+"&hnear=&radius=15000&t=m&vpsrc=6&ll="+data.data[0].camps[0].camp.geoLat+","+data.data[0].camps[0].camp.geoLang+"&spn=0.00228,0.006244&z=16&output=embed");
        
        //console.log(data.data[1].reviews);
        $("#cg_reviews").append('<h2>'+ campName + ' Reviews</h2>');
        $.each(data.data[1].reviews, function(i,item){
            ////console.log(item);          
            var image = setRatingImage(item.review.rating);
            $("#cg_reviews").append('<article class="review clearfix"><div class="userinfo"><img class="featured_member2" src="'+basePath+item.review.avatar+'" width="50" height="50" alt="Members Photo" /><br /><a class="username" href="user_profile.php?r='+item.review.reviewerId+'">'+item.review.firstName+'</a><br /></div><div class="article_content"><p>'+item.review.comments+'</p><img class="rating" src="'+basePath+image+'" /> <span class="date_of_stay">Date of Stay: '+item.review.date_of_stay+'</span> <img src="'+basePath+'images/thumbs_up.png" /> <img src="'+basePath+'images/thumbs_down.png" /></div></article>');
        });

        $.getJSON(basePath+'data/campoptions.php?c='+cid,function(data){
            //alert(data.options);
            $.each(data.options, function(i,item){
                $("#optionTable").append('<tr><td>'+item.option.oName+'</td><td class="right_value">'+item.option.optionValue+'</td></tr>');
            });
        });
        
         //mapInit(data.data[0].camps[0].camp.name);
    });
}

function mapInit(what) {
	
	geocoder = new google.maps.Geocoder();
	
	var myOptions = {zoom: 8,mapTypeId: google.maps.MapTypeId.ROADMAP };
	cgMap = new google.maps.Map(document.getElementById('cg_map'),myOptions);
	
	codeAddress(what);
}

function codeAddress(address) {
    
    geocoder = new google.maps.Geocoder();
    
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
      	console.log(results[0].geometry.location);
        cgMap.setCenter(results[0].geometry.location);
        
        var cgmarker = new google.maps.Marker({
            map: cgMap, 
            position: results[0].geometry.location
        });
      } else {
        alert("Geocode was not successful for the following reason: " + status);
      }
    });
}

/*
function setRatingImage(which){
	var image = 'images/fire/half.png';

	if(which >= 1 && which < 1.5){
        image = 'images/fire/one.png';
    } else if(which >= 1.5 && which < 2){
        image = 'images/fire/one-half.png';
    } else if(which >= 2 && which < 2.5){
        image = 'images/fire/two.png';
    } else if(which >= 2.5 && which < 3){
        image = 'images/fire/two-half.png';
    } else if(which >= 3 && which < 3.5){
        image = 'images/fire/three.png';
    } else if(which >= 3.5 && which < 4){
        image = 'images/fire/three-half.png';
    } else if(which >= 4 && which < 4.5){
        image = 'images/fire/four.png';
    } else if(which >= 4.5 && which < 5){
        image = 'images/fire/four-half.png';
    } else if(which >= 5){
        image = 'images/fire/five.png';
    }
    
    return image;
}
*/

    </script>
   
    