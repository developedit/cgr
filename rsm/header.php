<html>
<head>
    <title>RSM</title>
    <link rel="stylesheet" type="text/css" href="css/rsm.css" />
    <style type="text/css">
        body {
            margin: 0;
            padding: 0;
            background: #999999 url(images/page_bg.png) repeat-y center top;
        }

        #wrapper {
            width: 800px;
            margin-right: auto;
            margin-left: auto;
            border-right: 2px solid #000000;
            border-left: 2px solid #000000;
            background-color: #999999;
        }


        #main {
            display: inline; /* for IE */
            float: left;
            width: 459px;
            margin-left: 160px;
            padding-left: 10px;
            border-left: 1px dashed #999999;
            background-color: #FFFFFF;
        }


        #news {
            float: right;
            width: 160px;
            background-color: #FFFFFF;
        }


        #nav {
            float: left;
            width: 160px;
            margin-left: -630px;
            background-color: #FFFFFF;
        }


        #legal {
            clear: both;
            margin-right: 160px;
            padding: 5px 5px 20px 160px;
            border-top: 1px dashed #999999;
            font-weight: bold;
            color: #666666;
        }
    </style>

    <!-- <link type="text/css" href="../jq/css/custom-theme/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
         <script type="text/javascript" src="../jq/jquery-1.7.1.min.js"></script>
         <script type="text/javascript" src="../jq/jquery-ui-1.8.16.custom.min.js"></script> -->
</head>
<body>
<div id="wrapper">
    <div id="banner">
        <ul id="sitetools">
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">Subscribe</a> </li>
        </ul>
    </div>