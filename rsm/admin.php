<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Campground Reviews :: Admin</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le styles -->
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <link href="../assets/css/bootstrap-responsive.css" rel="stylesheet">
    <link href="../assets/css/custom.css" rel="stylesheet">

    <style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
      
      .sidebar-nav {
        padding: 5px 1px;
      }
    </style>

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../images/favicon.ico">
    <link rel="apple-touch-icon" href="../images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../images/apple-touch-icon-114x114.png">
  </head>

  <body data-spy="scroll" data-target=".subnav" data-offset="50">
	<!-- &rarr; -->
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="i-bar"></span>
            <span class="i-bar"></span>
            <span class="i-bar"></span>
          </a>
          <a class="brand" href="#">Campground Review</a>
          <div class="nav-collapse">
            <ul class="nav">
              <li id="cgmnu"><a onclick="switchContent('cg')">Campgrounds</a></li>
              <li id="revmnu"><a onclick="switchContent('reviews')">Reviews</a></li>
              <li id="rvrmnu"><a onclick="switchContent('reviewers')">Reviewers</a></li>
              <li id="adminmnu"><a onclick="switchContent('admins')">Admins</a></li>
            </ul>
			<ul class="nav pull-right">
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">Account <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="#navs" data-toggle="modal" id="setting"><i class="icon-cog"></i> Settings</a></li>
						<li><a href="#navbar"><i class="icon-user"></i> Logout</a></li>
					</ul>
				</li>
			</ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container">
    	<div class="navbar">
		  <div class="navbar-inner">
			<div class="container">
		      <ul class="nav">
				  <li>
				    <a href="#" id="addCamp">New</a>
				</li>
				 <li>
				    <a id="newOpt">Option Mgr</a>
				</li>
			</ul>
				<!--
<ul class="nav">
				  <li class="dropdown">
				    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Limit per page<b class="caret"></b></a>
				    <ul class="dropdown-menu">
				     <li><a href="#">10</a></li>
				     <li><a href="#">20</a></li>
				     <li><a href="#">30</a></li>
				     <li><a href="#">40</a></li>
				    </ul>
				  </li>
				</ul>
-->
				<form class="navbar-search pull-left" id="campSearchFrm">
				  <input type="text" class="search-query" id="campName" name="campName" onblur="searchCamps()" placeholder="Search">
				</form>
		    </div>
		  </div>
		</div>
		<div class="pagination pagination-centered" id="pages">
		  <ul id="pageLinks">
		    
		  </ul>
		</div>
	    <div id="cgs">
	      <table class="table table-striped table-bordered table-condensed" id="cglist_tbl">
			<thead>
			  <tr>
			    <th></th>
			    <th>Name</th>
			    <th>Location</th>
			    <th>Website</th>
			    <th>Geo</th>
			  </tr>
			</thead>
			<tbody id="listBody"></tbody>
		</table>
	    </div>
	    <div id="2" style="display:none">
	      <table class="table table-striped table-bordered table-condensed" id="rvlist_tbl">
			<thead>
			  <tr>
			    <th></th>
			    <th>Reviewer</th>
			    <th>Camp</th>
			  </tr>
			</thead>
			<tbody id="rvlistBody"></tbody>
		</table>
	    </div>
	    <div id="3" style="display:none">
	      <table class="table table-striped table-bordered table-condensed" id="reviewerlist_tbl">
			<thead>
			  <tr>
			    <th></th>
			    <th>Name</th>
			    <th>Email</th>
			  </tr>
			</thead>
			<tbody id="rvlistBody2"></tbody>
		</table>

	    </div>
	    <div id="4" style="display:none">
	      <p>Admins</p>
	    </div>
		 

    </div> <!-- /container -->
    
<!--    <div class="modal hide fade" id="myModal">-->
<!--	  <div class="modal-header">-->
<!--	    <a class="close" data-dismiss="modal">×</a>-->
<!--	    <h3>Modal header</h3>-->
<!--	  </div>-->
<!--	  <div class="modal-body">-->
<!--	    <p>One fine body…</p>-->
<!--	  </div>-->
<!--	  <div class="modal-footer">-->
<!--	    <a href="#" class="btn btn-primary">Save changes</a>-->
<!--	    <a href="#" class="btn" id="clsSetting">Close</a>-->
<!--	  </div>-->
<!--	</div>-->
	
<!-- popups	 -->
<?php include('newcampgroundfrm.html'); ?>
<?php include('editcampground.html'); ?>
<?php include('newreview.html'); ?>
<?php include('editreview.html'); ?>
<?php include('option_view.html'); ?>
<?php include('campOption_view.html'); ?>
<?php include('campreviews.html'); ?>

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBxbb7btwaq4ADW_y5cqQlM5GV2s3pyYB0&sensor=false"></script>
    <script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
    <script src="../assets/js/jquery.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>

    <script src="../js/adminApp.js"></script>
	
	<script>	
		$('#setting').click(function(){
			$('#myModal').modal('toggle');
		});
		
		$('#clsSetting').click(function(){
			$('#myModal').modal('toggle');
		});
		
		
	</script>
  </body>
</html>
