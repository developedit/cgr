    <aside>

      <section id="login_side">
          <form id="login_frm">
              <fieldset>
                  <label for="name">Username</label>
                  <input type="text" id="name" name="name" class="form-text">
              </fieldset>

              <fieldset>
                  <label for="password">Password</label>
                  <input type="password" id="password" name="password" class="form-text">
              </fieldset>
              <input type="submit" value="Login">
              <label><input type="checkbox"> Remember Me</label>
              <p class="smalltext">Not a Member? Register Today: <a href="/new/register.php"><button type="button">Register</button></a></p>
          </form>
          <div id="user_row" style="display: none">
              <span id="user_info"></span>
          </div>
      </section>

      <section id="social_icons">
        <ul>
          <li><a href="#"><img src="<?php echo $basePath; ?>/images/icon_ios.png" alot="Campground Report iOS App" /></a></li>
          <li><a href="#"><img src="<?php echo $basePath; ?>/images/icon_android.png" alot="Campground Report Android App" /></a></li>
          <li><a href="#"><img src="<?php echo $basePath; ?>/images/icon_facebook.png" alot="Campground Report on Facebook" /></a></li>
          <li><a href="#"><img src="<?php echo $basePath; ?>/images/icon_gplus.png" alot="Campground Report on Google Plus" /></a></li>
        </ul>
      </section>

      <section id="submit_review">
        <h3>Submit a Review</h3>
        <p>Submitting Reviews is Free, Easy, and helps to keep Campground Report a great place to find any information for later camping trips.</p>
        <div class="submit_button">
          <a id="subreview" class="submit"><span>Submit a Review</span></a>
        </div>
      </section>

      <section id="featured_member" class="clearfix">
        <h3>Featured Member</h3>

        <div id="member_info">
          <!--<img class="featured_member" src="images/avatar.jpg" alt="Members Photo" />
          <a class="username_large" href="#">Jeremy</a><br />
          <p class="side_location">Detroit, MI</p>
          <p class="side_reviews">Reviews: <a href="#">19</a> | Check-Ins: <a href="#">9</a></p>-->
        </div>

      </section>

    </aside>