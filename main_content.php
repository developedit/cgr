<form method="get" id="searchform" action="http://www.campgroundreport.com">
    <fieldset class="search">
        <input type="text" value="Search for a Campground, City, or State" class="box" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;" />
        <button class="btn" title="Submit Search">Search</button>
    </fieldset>
</form>

<img src="images/separator.jpg" alt="" width="615" height="24" class="or_separator" />


<div class="map_section">
    <div class="map_area">
        <img src="images/main_map.jpg" alt="" width="454" height="329" />
    </div><!-- .map_area -->
    <div class="state_selector">
        <ul id="states_lst">
            <li><a href="<?php echo $basePath; ?>/state/alabama">Alabama</a></li>
            <li><a href="<?php echo $basePath; ?>/state/alaska">Alaska</a></li>
            <li><a href="<?php echo $basePath; ?>/state/arizona">Arizona</a></li>
            <li><a href="<?php echo $basePath; ?>/state/arkansas">Arkansas</a></li>
            <li><a href="<?php echo $basePath; ?>/state/california">California</a></li>
            <li><a href="<?php echo $basePath; ?>/state/colorado">Colorado</a></li>
            <li><a href="<?php echo $basePath; ?>/state/connecticut">Connecticut</a></li>
            <li><a href="<?php echo $basePath; ?>/state/delaware">Delaware</a></li>
            <li><a href="<?php echo $basePath; ?>/state/florida">Florida</a></li>
            <li><a href="<?php echo $basePath; ?>/state/georgia">Georgia</a></li>
            <li><a href="<?php echo $basePath; ?>/state/hawaii">Hawaii</a></li>
            <li><a href="<?php echo $basePath; ?>/state/idaho">Idaho</a></li>
            <li><a href="<?php echo $basePath; ?>/state/illinois">Illinois</a></li>
            <li><a href="<?php echo $basePath; ?>/state/indiana">Indiana</a></li>
            <li><a href="<?php echo $basePath; ?>/state/iowa">Iowa</a></li>
            <li><a href="<?php echo $basePath; ?>/state/kansas">Kansas</a></li>
            <li><a href="<?php echo $basePath; ?>/state/kentucky">Kentucky</a></li>
            <li><a href="<?php echo $basePath; ?>/state/louisiana">Louisiana</a></li>
            <li><a href="<?php echo $basePath; ?>/state/maine">Maine</a></li>
            <li><a href="<?php echo $basePath; ?>/state/maryland">Maryland</a></li>
            <li><a href="<?php echo $basePath; ?>/state/massachusetts">Massachusetts</a></li>
            <li><a href="<?php echo $basePath; ?>/state/michigan">Michigan</a></li>
            <li><a href="<?php echo $basePath; ?>/state/minnesota">Minnesota</a></li>
            <li><a href="<?php echo $basePath; ?>/state/mississippi">Mississippi</a></li>
            <li><a href="<?php echo $basePath; ?>/state/missouri">Missouri</a></li>
            <li><a href="<?php echo $basePath; ?>/state/montana">Montana</a></li>
            <li><a href="<?php echo $basePath; ?>/state/nebraska">Nebraska</a></li>
            <li><a href="<?php echo $basePath; ?>/state/nevada">Nevada</a></li>
            <li><a href="<?php echo $basePath; ?>/state/new_hampshire">New Hampshire</a></li>
            <li><a href="<?php echo $basePath; ?>/state/new_jersey">New Jersey</a></li>
            <li><a href="<?php echo $basePath; ?>/state/new_mexico">New Mexico</a></li>
            <li><a href="<?php echo $basePath; ?>/state/new_york">New York</a></li>
            <li><a href="<?php echo $basePath; ?>/state/north_carolina">North Carolina</a></li>
            <li><a href="<?php echo $basePath; ?>/state/north_dakota">North Dakota</a></li>
            <li><a href="<?php echo $basePath; ?>/state/ohio">Ohio</a></li>
            <li><a href="<?php echo $basePath; ?>/state/oklahoma">Oklahoma</a></li>
            <li><a href="<?php echo $basePath; ?>/state/oregon">Oregon</a></li>
            <li><a href="<?php echo $basePath; ?>/state/pennsylvania">Pennsylvania</a></li>
            <li><a href="<?php echo $basePath; ?>/state/rhode_island">Rhode Island</a></li>
            <li><a href="<?php echo $basePath; ?>/state/south_carolina">South Carolina</a></li>
            <li><a href="<?php echo $basePath; ?>/state/south_dakota">South Dakota</a></li>
            <li><a href="<?php echo $basePath; ?>/state/tennessee">Tennessee</a></li>
            <li><a href="<?php echo $basePath; ?>/state/texas">Texas</a></li>
            <li><a href="<?php echo $basePath; ?>/state/utah">Utah</a></li>
            <li><a href="<?php echo $basePath; ?>/state/vermont">Vermont</a></li>
            <li><a href="<?php echo $basePath; ?>/state/virginia">Virginia</a></li>
            <li><a href="<?php echo $basePath; ?>/state/washington">Washington</a></li>
            <li><a href="<?php echo $basePath; ?>/state/west_virginia">West Virginia</a></li>
            <li><a href="<?php echo $basePath; ?>/state/wisconsin">Wisconsin</a></li>
            <li><a href="<?php echo $basePath; ?>/state/wyoming">Wyoming</a></li>
        </ul>
    </div><!-- .state_selector -->
</div><!-- .map_section -->

<div class="clear"></div>

<section class="recent_reviews">
    <h3>Recent Campground Reviews</h3>
    <ul id="reviewed">

    </ul>
</section>


<section class="top_rated">
    <h3>Top Rated Campgrounds</h3>
    <ul id="topcamps">

    </ul>
</section>

<div class="clear"></div>