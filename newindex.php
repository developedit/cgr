
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>Campground Reviews and RV Park Reviews</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <meta name="viewport" content="width=device-width,initial-scale=1">

  <link rel="stylesheet" href="css/style.css">

  <script src="js/libs/modernizr-2.0.min.js"></script>
  <script src="js/libs/respond.min.js"></script>
</head>
<body>
  <div id="header-container">
    <header class="wrapper">
      <div id="logo">
        <a href=""><img src="images/logo.png" width="268" height="103" alt="Campground Report" /></a>
      </div><!-- #logo --> 

      <div id="header_search">
          <form method="get" id="searchform" action="http://www.campgroundreport.com">
            <fieldset class="search">
              <input type="text" value="Search for a Campground, City, or State" class="box" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;" />
              <button class="btn" title="Submit Search">Search</button>
            </fieldset>
          </form>
      </div><!-- #header_search -->

      <div class="clear"></div>
      
        <nav>
          <ul>
            <li><a href="#" class="current">Home</a></li>
            <li><a href="state.php">States</a></li>
            <li><a href="#">Regions</a></li>
            <li class="register">
              <a href="register.php"><img src="images/register.png" alt="Register" /></a>
            </li>
          </ul>
        </nav>
    </header>
  </div>
  <div id="main" class="wrapper">

  <div id="left_column">

    <form method="get" id="searchform" action="http://www.campgroundreport.com">
      <fieldset class="search">
        <input type="text" value="Search for a Campground, City, or State" class="box" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;" />
        <button class="btn" title="Submit Search">Search</button>
      </fieldset>
    </form>

    <img src="images/separator.jpg" alt="" width="615" height="24" class="or_separator" />


    <div class="map_section">
      <div class="map_area">
        <img src="images/main_map.jpg" alt="" width="454" height="329" />
      </div><!-- .map_area -->
      <div class="state_selector">
        <ul>
          <li><a href="#">Alabama</a></li>
          <li><a href="#">Alaska</a></li>
          <li><a href="#">Arizona</a></li>
          <li><a href="#">Arkansas</a></li>
          <li><a href="#">California</a></li>
          <li><a href="#">Colorado</a></li>
          <li><a href="#">Connecticutt</a></li>
          <li><a href="#">Delaware</a></li>
          <li><a href="#">Florida</a></li>
          <li><a href="#">Georgia</a></li>
          <li><a href="#">Hawaii</a></li>
          <li><a href="#">Idaho</a></li>
          <li><a href="#">Illinois</a></li>
          <li><a href="#">Indiana</a></li>
          <li><a href="#">Iowa</a></li>
          <li><a href="#">Kansas</a></li>
          <li><a href="#">Kentucky</a></li>
          <li><a href="#">Louisiana</a></li>
          <li><a href="#">Alabama</a></li>
          <li><a href="#">Alabama</a></li>
          <li><a href="#">Alabama</a></li>
          <li><a href="#">Alabama</a></li>
          <li><a href="#">Alabama</a></li>
          <li><a href="#">Alabama</a></li>
        </ul>
      </div><!-- .state_selector -->
    </div><!-- .map_section -->

    <div class="clear"></div>

    <section class="recent_reviews">
      <h3>Recent Campground Reviews</h3>
      <ul>
        <li>
          <div class="title"><a href="#">Road Runner Travel Resort</a><img class="rating" src="images/fire/two-half.png" /></div>
          <p>in <a href="#">Oakeville</a>, <a href="#">Kansas</a> <em>by <a href="#">Andyman</a></em></p>
        </li>
        <li>
          <div class="title"><a href="#">Road Runner Travel Resort</a><img class="rating" src="images/fire/three-half.png" /></div>
          <p>in <a href="#">Oakeville</a>, <a href="#">Kansas</a> <em>by <a href="#">Andyman</a></em></p>
        </li>
        <li>
          <div class="title"><a href="#">Road Runner Travel Resort</a><img class="rating" src="images/fire/one-half.png" /></div>
          <p>in <a href="#">Oakeville</a>, <a href="#">Kansas</a> <em>by <a href="#">Andyman</a></em></p>
        </li>
        <li>
          <div class="title"><a href="#">Road Runner Travel Resort</a><img class="rating" src="images/fire/one.png" /></div>
          <p>in <a href="#">Oakeville</a>, <a href="#">Kansas</a> <em>by <a href="#">Andyman</a></em></p>
        </li>
        <li>
          <div class="title"><a href="#">Road Runner Travel Resort</a><img class="rating" src="images/fire/four-half.png" /></div>
          <p>in <a href="#">Oakeville</a>, <a href="#">Kansas</a> <em>by <a href="#">Andyman</a></em></p>
        </li>
        <li>
          <div class="title"><a href="#">Road Runner Travel Resort</a><img class="rating" src="images/fire/five.png" /></div>
          <p>in <a href="#">Oakeville</a>, <a href="#">Kansas</a> <em>by <a href="#">Andyman</a></em></p>
        </li>
      </ul>
    </section>


    <section class="top_rated">
      <h3>Top Rated Campgrounds</h3>
      <ul>
        <li>
          <div class="title"><a href="#">Road Runner Travel Resort</a><img class="rating" src="images/fire/four-half.png" /></div>
          <p>in <a href="#">Oakeville</a>, <a href="#">Kansas</a> <em>by <a href="#">Andyman</a></em></p>
        </li>
        <li>
          <div class="title"><a href="#">Road Runner Travel Resort</a><img class="rating" src="images/fire/four.png" /></div>
          <p>in <a href="#">Oakeville</a>, <a href="#">Kansas</a> <em>by <a href="#">Andyman</a></em></p>
        </li>
        <li>
          <div class="title"><a href="#">Road Runner Travel Resort</a><img class="rating" src="images/fire/three-half.png" /></div>
          <p>in <a href="#">Oakeville</a>, <a href="#">Kansas</a> <em>by <a href="#">Andyman</a></em></p>
        </li>
        <li>
          <div class="title"><a href="#">Road Runner Travel Resort</a><img class="rating" src="images/fire/two-half.png" /></div>
          <p>in <a href="#">Oakeville</a>, <a href="#">Kansas</a> <em>by <a href="#">Andyman</a></em></p>
        </li>
        <li>
          <div class="title"><a href="#">Road Runner Travel Resort</a><img class="rating" src="images/fire/one.png" /></div>
          <p>in <a href="#">Oakeville</a>, <a href="#">Kansas</a> <em>by <a href="#">Andyman</a></em></p>
        </li>
        <li>
          <div class="title"><a href="#">Road Runner Travel Resort</a><img class="rating" src="images/fire/half.png" /></div>
          <p>in <a href="#">Oakeville</a>, <a href="#">Kansas</a> <em>by <a href="#">Andyman</a></em></p>
        </li>
      </ul>
    </section>

    <div class="clear"></div>

    </div><!-- #left_column -->

        <aside>

      <section id="login_side">
        <form action="/">
            <fieldset>
              <label for="name">Username</label>
              <input type="text" id="name" class="form-text" />
            </fieldset>
            
            <fieldset>
              <label for="password">Password</label>
              <input type="password" id="password" class="form-text" />
            </fieldset>
            <input type="submit" value="Login" />
            <label><input type="checkbox" /> Remember Me</label>
            <p class="smalltext">Not a Member? Register Today: <a href="register.php"><button type="button">Register</button></a></p>
        </form>
      </section>

      <section id="social_icons">
        <ul>
          <li><a href="#"><img src="images/icon_ios.png" alot="Campground Report iOS App" /></a></li>
          <li><a href="#"><img src="images/icon_android.png" alot="Campground Report Android App" /></a></li>
          <li><a href="#"><img src="images/icon_facebook.png" alot="Campground Report on Facebook" /></a></li>
          <li><a href="#"><img src="images/icon_gplus.png" alot="Campground Report on Google Plus" /></a></li>
        </ul>
      </section>

      <section id="submit_review">
        <h3>Submit a Review</h3>
        <p>Submitting Reviews is Free, Easy, and helps to keep Campground Report a great place to find any information for later camping trips.</p>
        <div class="submit_button">
          <a href="single_review.php" class="submit"><span>Submit a Review</span></a>
        </div>
      </section>

      <section id="featured_member" class="clearfix">
        <h3>Featured Member</h3>

        <div>
          <img class="featured_member" src="images/avatar.jpg" alt="Members Photo" />
          <a class="username_large" href="#">Jeremy</a><br />
          <p class="side_location">Detroit, MI</p>
          <p class="side_reviews">Reviews: <a href="#">19</a> | Check-Ins: <a href="#">9</a></p>
        </div>

      </section>

    </aside>
    <div class="clear"></div>

  </div><!-- #main -->

  <div id="footer-container">
    <footer>
      
      <div id="footerlinks" class="column">
        <h4>Quicklinks</h4>
        <ul>
          <li><a href="#">Submit a Review</a></li>
          <li><a href="#">Claim a Campground</a></li>
          <li><a href="#">Sitemap</a></li>
          <li><a href="#">Contact Us</a></li>
          <li><a href="#">Privacy Policy</a></li>
        </ul>
      </div><!-- #footerlinks -->

      <div id="footer_right">
        <img src="images/logo.png" width="268" height="103" alt="Campground Report" />
        <p>Copyright &copy; 2000-2012 Campground Report. A <a href="http://www.socialknowledge.com">Social Knowledge</a> Site</p>
      </div><!-- #footer_right -->

      <div class="clear"></div>

    </footer>
  </div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/libs/jquery-1.6.2.min.js"><\/script>')</script>

<script src="js/script.js"></script>
<script>
  var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']]; // Change UA-XXXXX-X to be your site's ID
  (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];g.async=1;
  g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
  s.parentNode.insertBefore(g,s)}(document,'script'));
</script>

<!--[if lt IE 7 ]>
  <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.2/CFInstall.min.js"></script>
  <script>window.attachEvent("onload",function(){CFInstall.check({mode:"overlay"})})</script>
<![endif]-->

</body>
</html>