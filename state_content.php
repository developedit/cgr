
<div id="page_title">
  <h1><a href="#" id="state_link"></a></h1>
  <p class="state_description" id="state_description"></p>
</div><!-- #state_title -->

<!-- <form method="get" id="searchform" action="http://www.campgroundreport.com">
    <fieldset class="search">
        <input type="text" value="Search for a Campground, City, or State" class="box" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;" />
        <button class="btn" title="Submit Search">Search</button>
    </fieldset>
</form>

<img src="images/separator.jpg" alt="" width="615" height="24" class="or_separator" />
-->
<div class="map_section">
  <div class="state_map_area" id="state_map_area" style="height: 325px;">
     
  </div><!-- .map_area -->
  <div class="state_selector">
	  <!-- <select id="city_sel" width="50"/> -->
	    <ul id="cities">
	
	    </ul>
  </div><!-- .state_selector -->
</div><!-- .map_section -->
<div class="clear"></div>

<section class="recent_reviews">
    <h3>Recent Campground Reviews</h3>
    <ul id="reviewed">

    </ul>
</section>


<section class="top_rated">
    <h3>Top Rated Campgrounds</h3>
    <ul id="topcamps">

    </ul>
</section>
