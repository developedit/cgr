var all = {};
var count = 0;
var geocoder = new google.maps.Geocoder();

function getAllCamps(){
	$.getJSON('data/allcampgrounds.php' , function(data) {
	  	var items = [];
		//console.log(data.camps.length);
   		all = data.camps;
   		//console.log(all);
	});
}

function updateCamp(num,addy){
	console.log(num + ', ' + addy);
	geocoder.geocode( { 'address': addy}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
	      lat = results[0].geometry.location.$a;
		  lang = results[0].geometry.location.ab;
	      $.get('data/updategeo.php?la='+lat+'&lo='+lang+'&num='+num,function(data){
		      console.log(data);
		  });
      
      }
    });
}

function doUpdate(){
	var address = all[count].camp.address + ' ' + all[count].camp.city + ' ' + all[count].camp.state;
	updateCamp(all[count].camp.number,address);
	//console.log(all[0].camp.name);
	count++;
	if(count == 2240){
		clearInterval(timer);
	}
}

getAllCamps();

var timer = setInterval(function(){
	doUpdate();
},10000);