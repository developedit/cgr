//var basePath = 'http://dev.campgroundreport.com/';
var basePath = 'http://localhost/camping/';

$(function(){

	$("#logo").click(function(){
        window.location = basePath+'index.php';
    });

    $("#subreview").click(subAReview);
    $("#subreview_lnk").click(subAReview);

    $("#login_frm").submit(login);

    $("#logout_btn").click(function(e){
    	
       logOut();
    });
    
    $( "#dialog-modal" ).dialog({
        height: 140,
        modal: true,
        autoOpen: false
    });
    
    $("#searchform").submit(function(e){
    	e.preventDefault();
    	$.post("data/search.php", $("#searchform").serialize(),
	        function(data){
                var d = JSON.parse(data);

                if(d.camps.length){
                    $("#left_column").empty();
                    var d = JSON.parse(data);
                    $.each(d.camps, function(i,item){
                        var image = setRatingImage(item.camp.rank);

                        $('#left_column').append('<div class="title"><a href="'+basePath+'campground/'+item.camp.name+'">'+item.camp.name+'</a><img class="rating" src="'+image+'" /></div><p>in <a href="#">'+item.camp.city+'</a>, <a href="#">'+item.camp.state+'</a> <em>by <a href="#">Andyman</a></em></p>');
                    });
                }
	        }
	    );
    });

    $("li").click(function(event){
        //console.log(event);
    });
});

function subAReview(){
    if(checkUser()){
        $("#left_column").empty();
        $.get('single_review.php', function(data) {
            $("#left_column").html(data);
            user = JSON.parse(readCookie('reviewer'));

            $("#reviewer").val(user.id);
            stateDropdownList();
            //alert('Load was performed.');
        });
    } else {
        //$("#dialog-modal").dialog('open');
        TINY.box.show({html:'Please login to post a review',animate:false,close:false,boxid:'error',top:500});
    }
}

function setRatingImage(which){
	var image = 'images/fire/half.png';

	if(which >= 1 && which < 1.5){
        image = 'images/fire/one.png';
    } else if(which >= 1.5 && which < 2){
        image = 'images/fire/one-half.png';
    } else if(which >= 2 && which < 2.5){
        image = 'images/fire/two.png';
    } else if(which >= 2.5 && which < 3){
        image = 'images/fire/two-half.png';
    } else if(which >= 3 && which < 3.5){
        image = 'images/fire/three.png';
    } else if(which >= 3.5 && which < 4){
        image = 'images/fire/three-half.png';
    } else if(which >= 4 && which < 4.5){
        image = 'images/fire/four.png';
    } else if(which >= 4.5 && which < 5){
        image = 'images/fire/four-half.png';
    } else if(which >= 5){
        image = 'images/fire/five.png';
    }
    
    return image;
}

function getFeaturedMember(){
	$.getJSON("data/featuredmember.php", function(data){

        $.each(data.users, function(i,item){
        	////console.log(item);
        	var rev = new Object();
            rev.id = item.user.reviewerId;
            rev.name = item.user.firstName + " " + item.user.lastName;
            rev.email = item.user.email;
            rev.avatar = item.user.avatar;
            //usrObj = JSON.stringify(user);
            
            $("#member_info").append('<img class="featured_member" src="'+rev.avatar+'" alt="Members Photo" />');
            $("#member_info").append('<a class="username_large" href="#">'+item.user.firstName+'</a><br />');
            //<p class="side_location">Detroit, MI</p>

        });
    }); 
}

function stateList(){
    $.getJSON('js/states.json', function(data) {

        $.each(data.items, function(i,item){
            var begin = "<li>";
            $('#states_lst').append("<li><a>"+item.name+"</a></li>");
        });

    });
}


function stateLookUp(state){
	var s;
	var lat;
	var lang;
	var name;
	
	$.getJSON('js/states.json', function(data) {
		
        $.each(data.items, function(i,item){
            if(state === item.abbreviation){
	            s = item.abbreviation;
	            lat = item.lat;
	            lang = item.lang;
	            name = item.name;
	            currentState.abbv = s;
	            currentState.lat = lat;
	            currentState.lang = lang;
	            currentState.name = name;
	            //console.log(item);
            }
        });

        //window.location = currentState.name.toLowerCase()+'.php';
        window.location = basePath+'state/'+currentState.name.toLowerCase();
        
    });
    
}

function stateDropdownList(){
    $.getJSON('js/states.json', function(data) {
        $('#state').append('<option value="nothing">please select</option>');
        $.each(data.items, function(i,item){

            $('#state').append('<option value="'+item.abbreviation+'">'+item.name+'</option>');

        });

    });
}

function getTopReviewed(){
    $.getJSON('data/topcampgrounds.php', function(data) {

        $.each(data.camps, function(i,item){
            var image = setRatingImage(item.camp.rank);
            var cname = item.camp.name.replace(/ /g, "-");
           $('#topcamps').append('<li><div class="title"><a href="campground/'+cname+'">'+item.camp.name+'</a><img class="rating" src="'+image+'" /></div><p>in <a href="#">'+item.camp.city+'</a>, <a href="#">'+item.camp.state+'</a> </p></li>');

        });

    });
}

function getLatestReviewed(){
    $.getJSON('data/latestreviews.php', function(data) {

        $.each(data.reviews, function(i,item){
            var image = setRatingImage(item.review.rating);
            var cname = item.review.camp.replace(/ /g, "-");
            $('#reviewed').append('<li><div class="title"><a href="cg/'+cname+'">'+item.review.camp+'</a><img class="rating" src="'+image+'" /></div><p>in <a href="#">'+item.review.city+'</a>, <a href="#">'+item.review.state+'</a> <em>by <a href="user_profile.php?r='+item.review.reviewerId+'">'+item.review.firstName+'</a></em></p></li>');

        });

    });
}
//state_map_area

function showState(what){
	
	stateLookUp(what);
	
}

function getStateCampgrounds(){
	var state = $("#state").val();
	//alert(state);
	
	$.getJSON("data/statecampgrounds.php?s="+state,function(data){
	    //alert(data.cities);
	    $("#cg").empty();
	    $.each(data.camps, function(i,item){
	       
	        $("#cg").append('<option value="'+item.camp.number+'">'+item.camp.name+'</option>');
	    });
	});
}

function showCampground(camp){
    //console.log(replace_w_underscores(camp));
	//get the campground UI file
    window.location = basePath+'cg/'+replace_w_underscores(camp);
}

function getCampgroundData(camp){
	$.getJSON('data/campground.php?c='+camp, function(data) {
        
        $("#camp_name").html(data.data[0].camps[0].camp.name);
        $("#page_location").html(data.data[0].camps[0].camp.city + ', '+ data.data[0].camps[0].camp.state);
        $("#camp_desc").html(data.data[0].camps[0].camp.description);
        $("#camp_addy").html('<p>'+data.data[0].camps[0].camp.address+'<br />'+data.data[0].camps[0].camp.city + ', '+data.data[0].camps[0].camp.state + ' ' + data.data[0].camps[0].camp.zip + '<br />'+data.data[0].camps[0].camp.phone+'<br /><a target="_blank" href="'+data.data[0].camps[0].camp.website+'">Visit Website →</a></p>');
        $("#cg_reviews").empty();

        var cid = data.data[0].camps[0].camp.number;
        
        $.each(data.data[1].reviews, function(i,item){
            ////console.log(item);          
            var image = setRatingImage(item.review.rating);
            $("#cg_reviews").append('<article class="review clearfix"><div class="userinfo"><img class="featured_member" src="images/avatar.jpg" alt="Members Photo" /><br /><a class="username" href="#">Jeremy</a><br />Reviews: <a href="#">19</a></div><div class="article_content"><p>'+item.review.comments+'</p><img class="rating" src="'+image+'" /> <span class="date_of_stay">Date of Stay: '+item.review.date_of_stay+'</span> <img src="images/thumbs_up.png" /> <img src="images/thumbs_down.png" /></div></article>');
        });

        $.getJSON('data/campoptions.php?c='+cid,function(data){
            //alert(data.options);
            $.each(data.options, function(i,item){
                $("#optionTable").append('<tr><td>'+item.option.oName+'</td><td class="right_value">'+item.option.optionValue+'</td></tr>');
            });
        });
    });
}

function doSearch(){
	alert('YAH');
}

function showMain(){
    window.location = basePath+'index.php';
}

function showReview(what){
    //alert('huh');
    //$('#review_mdl').modal('show');
   
}

function showRegister(){
    $.get('register.php', function(data) {
        $('#left_column').html(data);
        //alert('Load was performed.');
    });
}

function register(){
    $.post("data/register.php", $("#register_frm").serialize(),
        function(data){
            var jdata = $.parseJSON(data);
            $.each(jdata.users, function(i,item){
                user.id = item.user.reviewerId;
                user.name = item.user.firstName + " " + item.user.lastName;
                user.email = item.user.email;
                usrObj = JSON.stringify(user);
                showUser(usrObj);
                showMain();
            });
        }
    );
}

function login(event){
	//alert('login');
    event.preventDefault();

    $.post("data/login.php", $("#login_frm").serialize(),
        function(data){
            var jdata = $.parseJSON(data);
            console.log(jdata.users[0].user);
            if(jdata.users[0].user != null){
	         	$.each(jdata.users, function(i,item){
	                user.id = item.user.reviewerId;
	                user.name = item.user.firstName + " " + item.user.lastName;
	                user.email = item.user.email;
	                usrObj = JSON.stringify(user);
	                user = usrObj;
	                showUser(usrObj);
	
	                if($('#remember:checked').val() != undefined){
	                    createCookie('reviewer',usrObj,7);
	                } else {
	                    createCookie('reviewer',usrObj,0);
	                }
	
	            });   
            } else {
	            alert('login failed');
            }
        }

    );
}

function getFeaturedMember(){
	$.getJSON("data/featuredmember.php", function(data){
		////console.log(data);
            //var jdata = $.parseJSON(data);

            $.each(data.users, function(i,item){
            	////console.log(item);
            	var rev = new Object();
                rev.id = item.user.reviewerId;
                rev.name = item.user.firstName + " " + item.user.lastName;
                rev.email = item.user.email;
                rev.avatar = item.user.avatar;
                //usrObj = JSON.stringify(user);
                
                $("#member_info").append('<img class="featured_member" src="'+rev.avatar+'" alt="Members Photo" />');
                $("#member_info").append('<a class="username_large" href="user_profile.php?r='+rev.id+'">'+item.user.firstName+'</a><br />');
                //<p class="side_location">Detroit, MI</p>

            });
        }

    ); 
}


function setRatingImage(which){
	var image = 'images/fire/half.png';

	if(which >= 1 && which < 1.5){
        image = 'images/fire/one.png';
    } else if(which >= 1.5 && which < 2){
        image = 'images/fire/one-half.png';
    } else if(which >= 2 && which < 2.5){
        image = 'images/fire/two.png';
    } else if(which >= 2.5 && which < 3){
        image = 'images/fire/two-half.png';
    } else if(which >= 3 && which < 3.5){
        image = 'images/fire/three.png';
    } else if(which >= 3.5 && which < 4){
        image = 'images/fire/three-half.png';
    } else if(which >= 4 && which < 4.5){
        image = 'images/fire/four.png';
    } else if(which >= 4.5 && which < 5){
        image = 'images/fire/four-half.png';
    } else if(which >= 5){
        image = 'images/fire/five.png';
    }
    
    return image;
}

function checkpwd(){
    //alert($("#password").val() + " == " + $("#confirm-password").val());
    if($("#password").val() != $("#confirm-password").val()){
        alert('Your passwords do not match.');
    }
}

function checkUserName(){
   if($("#name").val() != ""){
       $.get('data/checkname.php?n='+$("#name").val(), function(data) {
           if(data === "taken"){
               alert('That User Name is already in use, please choose another.');
               $("#name").focus();
           }
       });
   } else {
       alert('Please Enter a User Name.');
   }
}

function checkUser(){
	
	var ret = false;
	if(user){
	    if(user.id){
	        ret = true;
	    }
	}
	
    return ret;
}

function saveReview(){
	var hasError = false;
	$('#review_msg').empty();
	
	if($("#country").val() == 'nothing'){
		$('#review_msg').append('<li>please select a country</li>');
		hasError = true;
	} 
	
	if($("#state").val() == 'nothing'){
		$('#review_msg').append('<li>please select a state</li>');	
		hasError = true;
	}
	
	if($("#revname").val() == ''){
		$('#review_msg').append('<li>please choose a campground </li>');	
		hasError = true;
	}
	
	if($("#month_of_stay").val() == 'nothing'){
		$('#review_msg').append('<li>please select a month of stay</li>');	
		hasError = true;
	}
	
	if($("#year_of_stay").val() == 'nothing'){
		$('#review_msg').append('<li>please select a year</li>');	
		hasError = true;
	}
	
	if($("#rate").val() == ''){
		$('#review_msg').append('<li>please enter a rate</li>');	
		hasError = true;
	}
	
	if($("#sitetype").val() == 'nothing'){
		$('#review_msg').append('<li>please select a site type</li>');	
		hasError = true;
	}
	
	if($("#rating").val() == 'nothing'){
		$('#review_msg').append('<li>please select a rating</li>');	
		hasError = true;
	}
	
	if($("#review").val() == ''){
		$('#review_msg').append('<li>please enter a review</li>');	
		hasError = true;
	}
	
	if(hasError == false){
		$.post("data/savereview.php", $("#newReview_frm").serialize(),
		    function(data){
		        //alert("Data Loaded: " + data);
		        $('#review_msg').empty();
		        if(data == 1){
		            $('#review_div').addClass('ui-state-highlight');
		            $('#review_div').removeClass('ui-state-error');
		            $('#review_div').show();
		            $('#review_msg').append('<li>Review Saved</li>');
		            
		        } else {
		            $('#review_div').removeClass('ui-state-highlight');
		            $('#review_div').addClass('ui-state-error');
		            $('#review_div').show();
		            $('#review_msg').append('<li><strong>ERROR!</strong> Something went wrong. Please try again.</li>');
		        }
		
		    }
		);
	} else {
		$('#review_div').removeClass('ui-state-highlight');
		$('#review_div').addClass('ui-state-error');
		$('#review_div').show();
	}
}

//user display
function showUser(data){
    var u = JSON.parse(data);
    user = u;

    var html = 'Welcome, <a href="'+basePath+'user_profile.php?r='+ u.id+'">' + u.name + '</a>';
    $("#register").hide();
    $("#logout_btn").show();
    //$("#login_reg").attr('src',basePath+'images/logout.png');
    //$("#login_url").attr('href',basePath);

    $("#login_frm").hide();
    $("#user_info").html(html);
    $("#user_row").show();
}

function logOut(){

    $("#login_frm").show();
    $("#user_row").hide();
    eraseCookie("reviewer");
}

function replace_w_underscores(what){
    var wu = what.replace(/ /g, "_");
    return wu;
}

//cookie functions
function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}
