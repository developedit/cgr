//var basePath = 'http://dev.campgroundreport.com/';
var basePath = 'http://localhost/camping/';
var cities = new Array();
var campgrounds = new Array();
var currentState = {name:'',abbv:'',lat:'',lang:''};
var map;

var sPath = window.location.pathname;
var sPage = sPath.substring(sPath.lastIndexOf('/') + 1);
var what = sPage;
what = what.replace(/_/g, " ");
stateLookUp(what);
mapInit(what);

function init(){
    if(readCookie('reviewer')){
        user = readCookie('reviewer');
        //alert(user);
        showUser(user);
    } else {
        console.log('---- USER NOT FOUND ----');
    }
}

init();

function mapInit(what) {

    geocoder = new google.maps.Geocoder();

    var myOptions = {
        zoom: 8,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById('state_map_area'),myOptions);

    codeAddress(what);
}

function codeAddress(address) {

    geocoder.geocode( { 'address': address}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {



            var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location
            });
            map.setCenter(results[0].geometry.location);
        } else {
            alert("Geocode was not successful for the following reason: " + status);
        }
    });
}

function stateLookUp(state){
    var s;
    var lat;
    var lang;
    var name;

    $.getJSON(basePath+'js/states.json', function(data) {
        //console.log(data.items);
        $.each(data.items, function(i,item){
            //console.log('s: ' + state + ' n: ' + item.name);
            if(state.toLowerCase() === item.name.toLowerCase()){
                s = item.abbreviation;
                lat = item.lat;
                lang = item.lang;
                name = item.name;
                currentState.abbv = s;
                currentState.lat = lat;
                currentState.lang = lang;
                currentState['name'] = item.name;

            }
        });


       // what = currentState.abbv;
        setTitles(currentState.abbv);
        buildContent(currentState.abbv);
    });

}

function setTitles(state){
    console.log(currentState);
    $("#recent_state_rev_title").html('Recent ' + state + ' Campground Reviews');
    $("#top_state_rev_title").html('Top ' + state + ' Campground Reviews');
    $("#state_link").html(currentState.name + ' Campgrounds');
    $("#state_description").html('Find the best campgrounds in ' + currentState.name);
}

function buildContent(state){
    //change the tabs
    $("#home_tab").removeClass("current");
    $("#state_tab").addClass("current");
    //alert(what);
    //set the titles

    getFeaturedMember();

    $.getJSON(basePath+"data/statecities.php?s="+state,function(data){
        //alert(data.cities);
        $.each(data.cities, function(i,item){
            //alert(item.name);
            $("#city_sel").append('<option value="'+item.city.name+'">'+item.city.name+'</option>');
        });

    });


    //grab the Cgs for the state
    $.getJSON(basePath+"data/statecampgrounds.php?s="+state,function(data){
        //alert(data.cities);
        $.each(data.camps, function(i,item){
            // codeAddress(item.camp.address);
            var newLatLang = new google.maps.LatLng(item.camp.geoLat,item.camp.geoLang);
            var marker = new google.maps.Marker({
                map: map,
                position: newLatLang,
                title:item.camp.name,
                id:item.camp.number
            });
            google.maps.event.addListener(marker, 'click', function() {
                ////console.log(marker.id);
                showCampground(marker.id);
            });
            $("#cities").append('<li id="cmp'+item.camp.number+'" class="cmp" campname="'+item.camp.name+'">'+item.camp.name+'</li>');
        });
        $(".cmp").click(function(event){
            camp = $("#"+event.target.id).attr('campname');
            showCampground(camp);
        });
    });

    $.getJSON(basePath+"data/topcampsbystate.php?s="+state,function(data){
        //alert(data.cities);
        $.each(data.camps, function(i,item){
            var image = setRatingImage(item.camp.rank);
            var cname = item.camp.name;
            cname = cname.replace(/ /g, '_');
            $('#topcamps').append('<li><div class="title"><a href="'+basePath+'campground/'+cname+'">'+item.camp.name+'</a><img class="rating" src="'+basePath+image+'" /></div><p>in <a href="#">'+item.camp.city+'</a>, <a href="#">'+item.camp.state+'</a> </p></li>');

        });


    });

    $.getJSON(basePath+"data/lateststatereview.php?s="+state,function(data){
        //alert(data.cities);
        $.each(data.reviews, function(i,item){
            var image = setRatingImage(item.review.rating);
            $('#reviewed').append('<li><div class="title"><a href="'+basePath+'campground/'+item.review.camp+'">'+item.review.camp+'</a><img class="rating" src="'+basePath+image+'" /></div><p>in <a href="#">'+item.review.city+'</a>, <a href="#">'+item.review.state+'</a> <em>by <a href="'+basePath+'user_profile.php?r='+item.review.reviewerId+'">'+item.review.firstName+'</a></em></p></li>');
        });

    });
}

function logOut(){
    eraseCookie('reviewer');
    $("#side_login").show();
    $("#user_row").hide();
}

$(function() {

    $('#topcamps').empty();
    $('#reviewed').empty();
});
//alert(sPage);