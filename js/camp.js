var camp = getUrlVars()['c'];
var cgMap;

//alert(camp);

//$(function(){
	getCampgroundData(camp);
//});

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

function getCampgroundData(camp){
	$.getJSON('data/campground.php?c='+camp, function(data) {
        
        $("#camp_name").html(data.data[0].camps[0].camp.name);
        $("#page_location").html(data.data[0].camps[0].camp.city + ', '+ data.data[0].camps[0].camp.state);
        $("#camp_desc").html(data.data[0].camps[0].camp.description);
        $("#camp_addy").html('<p>'+data.data[0].camps[0].camp.address+'<br />'+data.data[0].camps[0].camp.city + ', '+data.data[0].camps[0].camp.state + ' ' + data.data[0].camps[0].camp.zip + '<br />'+data.data[0].camps[0].camp.phone+'<br /><a target="_blank" href="'+data.data[0].camps[0].camp.website+'">Visit Website →</a></p>');
        
        $("#cg_reviews").empty();
        
        var campName = data.data[0].camps[0].camp.name;
        //codeAddress(campName);
        campName = campName.replace(/\s/g,'+');;
        campName = campName.toLowerCase();
        
        $("#imap").attr('src',"http://maps.google.com/maps?ie=UTF8&q="+campName+"&fb=1&gl=us&hq="+campName+"&hnear=&radius=15000&t=m&vpsrc=6&ll="+data.data[0].camps[0].camp.geoLat+","+data.data[0].camps[0].camp.geoLang+"&spn=0.00228,0.006244&z=16&output=embed");
        
        //console.log(data.data[1].reviews);
        $("#cg_reviews").append('<h2>'+ data.data[0].camps[0].camp.name + ' Reviews</h2>');
        $.each(data.data[1].reviews, function(i,item){
            ////console.log(item);          
            var image = setRatingImage(item.review.rating);
            $("#cg_reviews").append('<article class="review clearfix"><div class="userinfo"><img class="featured_member" src="images/avatar.jpg" alt="Members Photo" /><br /><a class="username" href="#">Jeremy</a><br />Reviews: <a href="#">19</a></div><div class="article_content"><p>'+item.review.comments+'</p><img class="rating" src="'+image+'" /> <span class="date_of_stay">Date of Stay: '+item.review.date_of_stay+'</span> <img src="images/thumbs_up.png" /> <img src="images/thumbs_down.png" /></div></article>');
        });
        
         //mapInit(data.data[0].camps[0].camp.name);
    });
    
    $.getJSON('data/campoptions.php?c='+camp,function(data){
    	//alert(data.options);
    	$.each(data.options, function(i,item){
    		$("#optionTable").append('<tr><td>'+item.option.oName+'</td><td class="right_value">'+item.option.optionValue+'</td></tr>');
    	});
    });
    
   
}

function mapInit(what) {
	
	geocoder = new google.maps.Geocoder();
	
	var myOptions = {zoom: 8,mapTypeId: google.maps.MapTypeId.ROADMAP };
	cgMap = new google.maps.Map(document.getElementById('cg_map'),myOptions);
	
	codeAddress(what);
}

function codeAddress(address) {
    
    geocoder = new google.maps.Geocoder();
    
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
      	//console.log(results[0].geometry.location);
        cgMap.setCenter(results[0].geometry.location);
        
        var cgmarker = new google.maps.Marker({
            map: cgMap, 
            position: results[0].geometry.location
        });
      } else {
        alert("Geocode was not successful for the following reason: " + status);
      }
    });
}
